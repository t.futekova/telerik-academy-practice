package pages.trello;

import org.openqa.selenium.WebDriver;

public class LogoutPage extends BaseTrelloPage{

    public LogoutPage(WebDriver driver) {
        super(driver, "trello.logoutUrl");
    }

    public void logoutUser(){
        navigateToPage();

        actions.waitForElementVisible("trello.logoutPage.logoutButton");
        actions.clickElement("trello.logoutPage.logoutButton");
    }

}
