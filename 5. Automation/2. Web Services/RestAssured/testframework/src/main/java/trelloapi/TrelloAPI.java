package trelloapi;

import com.telerikacademy.testframework.Utils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import trelloapi.Models.BoardModel;
import trelloapi.Models.CardModel;
import trelloapi.Models.ListModel;

import java.util.ArrayList;
import java.util.List;

public class TrelloAPI {
    private String apiKey;
    private String apiToken;
    private String trelloApiUrl;

    public void authenticate(String userNameKey){
        String fullUserKey = "trello.users." + userNameKey;
        trelloApiUrl = Utils.getConfigPropertyByKey("trello.apiUrl");
        apiKey = Utils.getConfigPropertyByKey(fullUserKey + ".apiKey");
        apiToken = Utils.getConfigPropertyByKey(fullUserKey + ".token");
    }

    public BoardModel createBoard(String name, boolean createDefaultLists){
        String createUrl = trelloApiUrl + "/1/boards/?name="+ name + "&key=" + apiKey + "&token=" + apiToken + "&defaultLists=" + createDefaultLists;
        Utils.LOG.info("POST URL: " + createUrl);

        BoardModel responseBoard =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .post(createUrl)
                        .then()
                        .statusCode(200)
                        .extract().response()
                        .as(BoardModel.class);

        return responseBoard;
    }
    public List<BoardModel> getAllBoards(){
        String getUrl = trelloApiUrl + "/1/members/me/boards?fields=name,url&key=" + apiKey + "&token=" + apiToken;
        Utils.LOG.info("GET URL: " + getUrl);

        Response response =
            RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .extract().response();

        List<BoardModel> boards = response.jsonPath().getList("..", BoardModel.class);
        return boards;
    }
    public BoardModel getBoard(String name){
        List<BoardModel> boards = getAllBoards();
        for (BoardModel board : boards) {
            if(board.name.equals(name)){
                return board;
            }
        }
        return null;
    }
    public void deleteBoard(BoardModel createdBoard) {
        String deleteUrl = trelloApiUrl + "/1/boards/" + createdBoard.id + "?key=" + apiKey + "&token=" + apiToken;
        Utils.LOG.info("Deleting at URL: " + deleteUrl);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete(deleteUrl)
                .then()
                .statusCode(200);
    }

    public CardModel createCardInList(String cardName, BoardModel createdBoard, String listName){
        String listId = getListInBoard(createdBoard, listName).id;

        String createUrl = trelloApiUrl + "/1/cards/?name=" + cardName + "&key=" + apiKey + "&token=" + apiToken + "&idList=" + listId;
        Utils.LOG.info("POST URL: " + createUrl);

        CardModel responseCard =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .post(createUrl)
                        .then()
                        .statusCode(200)
                        .extract().response()
                        .as(CardModel.class);

        return responseCard;
    }

    public void moveCardToList(CardModel card, BoardModel createdBoard, String listName){
        String listId = getListInBoard(createdBoard, listName).id;
        String cardId = card.id;

        String createUrl = trelloApiUrl + "/1/cards/"+ cardId + "?key=" + apiKey + "&token=" + apiToken + "&idList=" + listId;
        Utils.LOG.info("PUT URL: " + createUrl);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .put(createUrl)
                .then()
                .statusCode(200)
                .extract().response()
                .as(CardModel.class);

    }

    public List<ListModel> getAllListsInBoard(BoardModel createdBoard){
        String boardId = createdBoard.id;

        String detUrl = trelloApiUrl + "/1/boards/"+ boardId + "/lists?key=" + apiKey + "&token=" + apiToken;
        Utils.LOG.info("GET URL: " + detUrl);

        Response response =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .get(detUrl)
                        .then()
                        .statusCode(200)
                        .extract().response();

        List<ListModel> lists = response.jsonPath().getList("..", ListModel.class);
        return lists;
    }

    public ListModel getListInBoard(BoardModel createdBoard, String listName){
        List<ListModel> lists = getAllListsInBoard(createdBoard);
        for (ListModel list : lists) {
            if(list.name.equals(listName)){
                return list;
            }
        }
        return null;
    }
}
