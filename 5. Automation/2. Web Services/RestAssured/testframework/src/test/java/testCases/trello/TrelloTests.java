package testCases.trello;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.trello.BoardPage;
import pages.trello.BoardsPage;
import pages.trello.LoginPage;
import pages.trello.LogoutPage;
import trelloapi.Models.BoardModel;
import trelloapi.Models.CardModel;
import trelloapi.TrelloAPI;

public class TrelloTests extends BaseTest {
    private BoardModel createdBoard;
    private TrelloAPI trelloAPI;
    private CardModel createdCard;

    @Before
    public void beforeTest(){
        trelloAPI = new TrelloAPI();
        trelloAPI.authenticate("teodorafutekova");
        createdBoard = trelloAPI.createBoard("Trello Board - " + System.currentTimeMillis(), true);

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("teodorafutekova");
    }

    @Test
    public void openExistingBoard_When_BoardNameClicked() {
        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name);

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.assertListExists("To Do");
    }

    @Test
    public void createNewCardInExistingBoard_When_CreateCardClicked() {
        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name);

        BoardPage boardPage = new BoardPage(actions.getDriver());
        String cardName = "New Task - " + System.currentTimeMillis();
        boardPage.addCardToList(cardName, "To Do");

        boardPage.assertCardExists(cardName);
    }

    @Test
    public void moveCardBetweenStates_When_DragAndDropIsUsed() {
        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name);

        String cardName = "New Task - " + System.currentTimeMillis();
        createdCard = trelloAPI.createCardInList(cardName, createdBoard, "To Do");
        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.moveCardToList(createdCard, "Doing");

        boardPage.assertCardIsInList("Doing", cardName);
    }

    @Test
    public void DeleteBoard_When_DeleteButtonIsClicked() {
        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name);

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.deleteBoard();

        boardPage.assertBoardCannotBeFound();
    }

    @After
    public void afterTest(){
        if (trelloAPI.getBoard(createdBoard.name)!=null){
            trelloAPI.deleteBoard(createdBoard);
        }
        LogoutPage logoutPage = new LogoutPage(actions.getDriver());
        logoutPage.logoutUser();
    }
}
