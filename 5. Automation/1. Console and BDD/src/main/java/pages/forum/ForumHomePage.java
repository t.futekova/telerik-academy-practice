package pages.forum;

import com.telerikacademy.forumframework.Utils;
import com.telerikacademy.forumframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumHomePage extends BasePage {

    public ForumHomePage(WebDriver driver) {
        super(driver, "forum.homePage");
    }

    public void authenticateRegisteredUser(String email, String password){
        if(actions.isElementVisible("forum.homePage.logInButton")){
            actions.clickElement("forum.homePage.logInButton");

            if(!actions.isElementVisible("forum.homePage.userAvatar")){
                actions.assertNavigatedUrl("forum.logInPage");

                ForumLogInPage logInPage = new ForumLogInPage(actions.getDriver());
                logInPage.fillInCredentials(email, password);
            }
        }
    }

    public void logOutAuthenticatedUser(){
        actions.clickElement("forum.homePage.userAvatar");
        actions.isElementVisible("forum.homePage.userPreferencesButton");
        actions.clickElement("forum.homePage.userPreferencesButton");
        actions.isElementVisible("forum.homePage.logOutButton");
        actions.clickElement("forum.homePage.logOutButton");
    }

    public void clearDraft(){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey("forum.homePage.newTopicButton")));
        if(element.getAttribute("aria-label").equals("Open Draft")){
            actions.clickElement("forum.homePage.newTopicButton");
            actions.clickElement("forum.homePage.cancelDraftButton");
            actions.waitForElementVisible("forum.homePage.confirmCancelDraftButton");
            actions.clickElement("forum.homePage.confirmCancelDraftButton");
        }
    }

    public void circumventSpamSecurity(){
        if(isSpamSecurityPopUpDisplayed()){
            actions.clickElement("forum.homePage.spamPopUpOKButton");
            actions.typeValueInField("7.003", "forum.homePage.descriptionField");
            actions.clickElement("forum.homePage.createTopicButton");
        }
    }

    private boolean isSpamSecurityPopUpDisplayed(){
        return actions.isElementVisible("forum.homePage.spamPopUp");
    }

}
