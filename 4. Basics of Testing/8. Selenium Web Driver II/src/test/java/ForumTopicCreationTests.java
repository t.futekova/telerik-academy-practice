import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.forum.telerikacademy.ForumHomePage;
import pages.forum.telerikacademy.ForumLogInPage;
import pages.forum.telerikacademy.ForumNewTopicPage;

import java.util.concurrent.TimeUnit;

public class ForumTopicCreationTests {

    private final String FORUM_HOME_PAGE_URL = "https://stage-forum.telerikacademy.com/";
    private final String FORUM_LOGIN_PAGE_URL = "https://auth.telerikacademy.com/account/login";
    private final String EMAIL = "kravdit@gmail.com";
    private final String PASSWORD = "buddyGroup3";

    private WebDriver webdriver;
    private WebDriverWait wait;

    @BeforeClass
    public static void classSetUp(){
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setUp(){
        webdriver = new ChromeDriver();
        webdriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(webdriver, 10);
        webdriver.get(FORUM_HOME_PAGE_URL);
    }

    @After
    public void tearDown(){
        webdriver.quit();
    }

    @Test
    public void authenticateRegisteredUser_When_LogInFormIsFilledWithValidCredentials(){
        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));

        Assert.assertEquals(webdriver.getCurrentUrl(), FORUM_HOME_PAGE_URL);

        homePage.getLogInButton().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("next")));

        Assert.assertTrue(webdriver.getCurrentUrl().contains(FORUM_LOGIN_PAGE_URL));

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.logInRegisteredUser(EMAIL, PASSWORD);

        Assert.assertTrue("Create New Topic functionality isn't enabled after user sign in", homePage.getNewTopicButton().isDisplayed());
    }

    @Test
    public void validationPopUpsAreTriggered_When_TopicTitleAndDescriptionAreEmpty(){
        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));
        homePage.getLogInButton().click();

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.logInRegisteredUser(EMAIL, PASSWORD);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        homePage.createNewTopic("", "");

        Assert.assertTrue("Title validation popup isn't displayed", homePage.getValidateTitlePopup().isDisplayed());
        Assert.assertTrue("Description validation popup isn't displayed", homePage.getValidateDescriptionPopup().isDisplayed());
    }

    @Test
    public void createNewTopic_When_ValidTitleAndDescriptionAreProvided(){
        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));
        homePage.getLogInButton().click();

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.logInRegisteredUser(EMAIL, PASSWORD);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        homePage.createNewTopic("Selenium Web Driver Test", "Selenium Web Driver Test");
        ForumNewTopicPage newTopicPage = new ForumNewTopicPage(webdriver);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@title='begin composing a reply to this topic']")));
        Assert.assertTrue("Reply button isn't displayed", newTopicPage.getTopicReplyButton().isDisplayed());
    }

}
