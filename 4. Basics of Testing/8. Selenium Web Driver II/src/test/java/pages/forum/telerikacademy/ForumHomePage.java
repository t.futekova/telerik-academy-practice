package pages.forum.telerikacademy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class ForumHomePage {

    private int count = 1;

    private WebDriver webdriver;

    public ForumHomePage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getLogInButton(){
        return webdriver.findElement(By.xpath("//button[contains(.,'Log In')]"));
    }

    public WebElement getNewTopicButton() {
        return webdriver.findElement(By.id("create-topic"));
    }

    public WebElement getCreateTopicButton() {
        return webdriver.findElement(By.xpath("//button[contains(.,'Create Topic')]"));
    }

    public WebElement getValidateTitlePopup() {
        return webdriver.findElement(By.xpath("//div[contains(., 'Title is required')and@class='popup-tip bad ember-view']"));
    }

    public WebElement getValidateDescriptionPopup() {
        return webdriver.findElement(By.xpath("//div[contains(., 'Post can’t be empty')and@class='popup-tip bad ember-view']"));
    }

    public WebElement getCancelDraftButton() {
        return webdriver.findElement(By.xpath("//a[contains(.,'cancel')]"));
    }

    public WebElement getConfirmCancelDraftButton() {
        return webdriver.findElement(By.xpath("//button[contains(.,'Yes, abandon')]"));
    }

    public WebElement getTitleField() {
        return webdriver.findElement(By.id("reply-title"));
    }

    public WebElement getDescriptionField() {
        return webdriver.findElement(By.xpath("//textarea[contains(@aria-label,'Type here')]"));
    }

    public WebElement getSpamPopUpOKButton() {
        return webdriver.findElement(By.xpath("//a[contains(.,'OK')]"));
    }

    public void createNewTopic(String title, String description){
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        clearDraft();
        getNewTopicButton().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@role='form']")));
        getTitleField().sendKeys(title);
        getDescriptionField().sendKeys(description);
        getCreateTopicButton().click();
        if(isSpamSecurityPopUpDisplayed()){
            getSpamPopUpOKButton().click();
            getDescriptionField().sendKeys(String.valueOf(count));
            count++;
            getCreateTopicButton().click();
        }
    }

    private void clearDraft(){
        if(getNewTopicButton().getAttribute("aria-label").equals("Open Draft")) {
            getNewTopicButton().click();
            getCancelDraftButton().click();
            getConfirmCancelDraftButton().click();
        }
    }

    private boolean isSpamSecurityPopUpDisplayed(){
        try {
            webdriver.findElement(By.xpath("//div[contains(@class, 'bootbox')]"));
            return true;
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

}
