package pages.forum.telerikacademy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumNewTopicPage {

    private WebDriver webdriver;

    public ForumNewTopicPage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getTopicReplyButton(){
        return webdriver.findElement(By.xpath("//button[@title='begin composing a reply to this topic']"));
    }

}
