package pages.forum.telerikacademy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumLogInPage {

    private WebDriver webdriver;

    public ForumLogInPage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getEmailInput(){
        return webdriver.findElement(By.id("Email"));
    }

    public WebElement getPasswordInput(){
        return webdriver.findElement(By.id("Password"));
    }

    public WebElement getSignInButton(){
        return webdriver.findElement(By.id("next"));
    }

    public void logInRegisteredUser(String email, String password){
        getEmailInput().sendKeys(email);
        getPasswordInput().sendKeys(password);
        getSignInButton().click();
    }

}
