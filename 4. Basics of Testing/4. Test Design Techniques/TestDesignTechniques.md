# Test Design Techniques 

## Forum testing prerequisites and general info

The following design techniques have been abbreviated:
   Equivalence partitioning    - EP
   Boundary Value Analysis     - BVA
   Decision Table              - DT
   Pairwise Testing            - PT

| When is a user allowed to post in a forum?|   |   |   |   |
|-------------------------------------------|---|---|---|---|
| The user is registered in the forum       | Y | Y | N | N |
| The user is logged in                     | Y | N | - | N |
| The user can post - create topic/comment  | Y | N | N | N |

|A logged out user cannot create a forum topic or comment.   |   Prio 1   |   DT
|A logged in user can create a forum topic or comment.       |   Prio 1   |   DT

## Topic creation

|Title                                                                                                           |Priority    |Technique  |
|----------------------------------------------------------------------------------------------------------------|------------|-----------|
|A user cannot post a topic with an empty title.                                                                 |   Prio 1   |   BVA     |
|A user cannot post a topic with a title with less than 5 characters.                                            |   Prio 1   |   BVA     |
|A user cannot post a topic with an empty body.                                                                  |   Prio 1   |   BVA     |
|A user cannot post a topic with a body with less than 10 characters.                                            |   Prio 1   |   BVA     |
|A user can post a topic with a title with more than 5, body with more than 10 characters and no category.       |   Prio 1   |   PT, BVA |
|A user can post a topic with a title with more than 5, body with more than 10 characters in a forum category.   |   Prio 1   |   PT, BVA |
|A user can post a topic with a title with more than 5 characters and body with an attached image.               |   Prio 2   |   PT, BVA |
|A topic author can edit topic with valid input for title and body.                                              |   Prio 2   |   PT, BVA |
|A topic author cannot edit topic with invalid input for title and body.                                         |   Prio 2   |   PT, BVA |

## Commenting

|Title                                                                       |Priority    |Technique  |
|----------------------------------------------------------------------------|------------|-----------|
|A user cannot post an empty comment under a topic.                          |   Prio 1   |   BVA     |
|A user cannot post a comment with less than 3 characters under a topic.     |   Prio 1   |   BVA     |
|A user can post a comment with more than 3 characters under a topic.        |   Prio 1   |   BVA     |
|A user can post a comment with an attached image.                           |   Prio 2   |   BVA     |

## Notifications

|When does a registered forum user receive e-mail notifications for a topic?|   |   |   |   |   |   |   |   |
|---------------------------------------------------------------------------|---|---|---|---|---|---|---|---|
| The user is the topic author / is subscribed to the topic                 | Y | Y | Y | Y | N | N | N | N |
| The user has e-mail notifications turned on                               | Y | Y | N | N | Y | Y | N | N |
| Another user interacts with the topic -                                   | Y | N | Y | N | Y | N | Y | N |
| comments on topic / likes or responds to comment                          |   |   |   |   |   |   |   |   |
| The user receives an e-mail notification                                  | Y | N | N | N | N | N | N | N |

|Title                                                                                                                                     |Priority    |Technique  |
|------------------------------------------------------------------------------------------------------------------------------------------|------------|-----------|
|A topic author / a subscribed user with enabled notifications receives an e-mail after another user interacts with the topic.             |   Prio 3   |   DT      |
|A topic author / a subscribed user with disabled notifications does not receives an e-mail after another user interacts with the topic.   |   Prio 3   |   DT      |
|A user with enabled notifications does not receives e-mails for topics the user hasn't authored or isn't subscribed to.                   |   Prio 3   |   DT      |