#A user attempts to create a topic with empty title and description
##Go to forum
type(Key.META)
wait(1)
type("Chrome" + Key.ENTER)
chromeHasLoadedIndicator = "chromeLoadedRibbon.png"
wait(chromeHasLoadedIndicator)
keyDown(Key.CTRL + Key.SHIFT + "n")
keyUp()
wait(1)
type("stage-forum.telerikacademy.com" + Key.ENTER)

##Log in
logInButton = "logInButton.png"
wait(logInButton, 10)
click(logInButton)
chromeIncognitoHasLoadedIndicator = "chromeIncognitoLoadedRibbon.png"
wait(chromeIncognitoHasLoadedIndicator)
emailField = "emailField.png"
wait(emailField, 10)
type("kravdit@gmail.com")

passwordField = "passwordField.png"
click(passwordField)
type("buddyGroup3")
signInButton = "signInButton.png"
click(signInButton)

##Validate topic title and description
newTopicButton = "newTopicButton.png"
wait(newTopicButton, 10)
click(newTopicButton)
createTopicButton = "createTopicButton.png"
click(createTopicButton)
titleValidationPopUp = "titleValidationPopUp.png"
postValidationPopUp = "postValidationPopUp.png"
wait(titleValidationPopUp).highlight(1)
wait(postValidationPopUp).highlight(1)
popup("Testing passed!")



