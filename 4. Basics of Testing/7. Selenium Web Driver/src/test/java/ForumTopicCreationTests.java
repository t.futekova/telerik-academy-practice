import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.forum.telerikacademy.ForumHomePage;
import pages.forum.telerikacademy.ForumLogInPage;

import java.util.concurrent.TimeUnit;

public class ForumTopicCreationTests {

    private WebDriver webdriver;
    private WebDriverWait wait;

    @BeforeClass
    public static void classSetup(){
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup(){
        webdriver = new ChromeDriver();
        webdriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(webdriver, 10);
        webdriver.get("http://stage-forum.telerikacademy.com/");
    }

    @After
    public void tearDown(){
        webdriver.quit();
    }

    @Test
    public void validateTopic_TitleAndDescriptionCannotBeEmpty(){
        ForumHomePage homePage = new ForumHomePage(webdriver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'Log In')]")));
        homePage.getLogInButton().click();

        ForumLogInPage logInPage = new ForumLogInPage(webdriver);
        logInPage.signIn();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        homePage.clearDraft();

        homePage.createNewTopic("", "");

        Assert.assertTrue("Title validation popup isn't displayed", homePage.getValidateTitlePopup().isDisplayed());
        Assert.assertTrue("Description validation popup isn't displayed", homePage.getValidateDescriptionPopup().isDisplayed());
    }

}
