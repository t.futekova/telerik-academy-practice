package pages.forum.telerikacademy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumHomePage {
    private WebDriver webdriver;

    public ForumHomePage(WebDriver webdriver){
        this.webdriver = webdriver;
    }

    public WebElement getLogInButton(){
        return webdriver.findElement(By.xpath("//button[contains(.,'Log In')]"));
    }

    public WebElement getNewTopicButton() {
        return webdriver.findElement(By.id("create-topic"));
    }

    public WebElement getCreateTopicButton() {
        return webdriver.findElement(By.xpath("//button[contains(.,'Create Topic')]"));
    }

    public WebElement getValidateTitlePopup() {
        return webdriver.findElement(By.xpath("//div[contains(., 'Title is required')and@class='popup-tip bad ember-view']"));
    }

    public WebElement getValidateDescriptionPopup() {
        return webdriver.findElement(By.xpath("//div[contains(., 'Post can’t be empty')and@class='popup-tip bad ember-view']"));
    }

    public WebElement getCancelDraftButton() {
        return webdriver.findElement(By.xpath("//a[contains(.,'cancel')]"));
    }

    public WebElement getConfirmCancelDraftButton() {
        return webdriver.findElement(By.xpath("//button[contains(.,'Yes, abandon')]"));
    }

    public WebElement getTitleField() {
        return webdriver.findElement(By.id("reply-title"));
    }

    public WebElement getDescriptionField() {
        return webdriver.findElement(By.xpath("//textarea[contains(@aria-label,'Type here')]"));
    }

    public void clearDraft(){
        if (getNewTopicButton().getAttribute("aria-label").equals("Open Draft")) {
            getNewTopicButton().click();
            getCancelDraftButton().click();
            getConfirmCancelDraftButton().click();
        }
    }

    public void createNewTopic(String title, String description){
        getNewTopicButton().click();
        getTitleField().sendKeys(title);
        getDescriptionField().sendKeys(description);
        getCreateTopicButton().click();
    }
}
