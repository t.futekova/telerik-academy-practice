Issue 1:
Severity High
Title: Empty "About Us" and "How to book" pages in Russian
Description: The "About Us" and "How to book" pages in the Russian version of the site are empty

Steps to reproduce:
	1. Go to https://phptravels.net/ru
	2. Scroll to the bottom of the page and click on "About Us"/"How to book"

Expected result: You are redirected to a page with additional information.

Actual result: You are redirected to an empty page.

Environment: Browser Google Chrome Version 90.0.4430.85 (Official Build) (64-bit)

Issue 2:
Severity Low
Title: German language gunctionality doesn't translate text to German
Description: When language is changed to German the text isn't translated

Steps to reproduce:
	1. Go to https://phptravels.net/en
	2. Click on "English" button located in the top right page corner
	3. Select "German" from the dropdown menu

Expected result: The page's text is translated to German.

Actual result: The page's text remains in English.

Environment: Browser Google Chrome Version 90.0.4430.85 (Official Build) (64-bit)

Issue 3:
Severity Blocking
Title: Search functionality doesn't validate fields before execution
Description: When user doesn't enter a destination and clicks Search button system doesn't display a pop up prompting User to fill in destination

Steps to reproduce:
	1. Go to https://phptravels.net/home
	2. Click on "+" button under "Adults" in the search bar to change number of adults from 2 to 3
	3. Click on the "SEARCH" button

Expected result: The search bar displays a pop-up prompt to fill in "DESTINATION" field.

Actual result: The "SEARCH" button displays animation as if command is being executed but nothing happens.

Environment: Browser Google Chrome Version 90.0.4430.85 (Official Build) (64-bit)