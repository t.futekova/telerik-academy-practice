# Test Plan
The purpose of this document is to outline the testing principles used for the "forum.telerikacademy.com"'s topic creation functionality
to ensure that after the testing, the customer is provided with a ready functionality without errors and with good readability.

## Testing types
* Manual testing
    - Aim - Verifying the tested functionality operates according to Client's requirements
    - Points to be checked:
        - The input data validity
        - Allowed values for the data fields
        - Invalid input values for the data field
        - Outbound links
        - Attached content proper rendering
        - Searchability by tags
    - Test cases: Must include title, description, steps to reproduce, expected result, priority and owner
    - Test case management tool: TestRail

* Usability testing
    - Aim - Defining user's ability to learn to operate and prepare inputs for the tested functionality.
    - Points to be checked:
        - Buttons, shapes, and fields are convenient for use
        - There is an access to the topic creation functionality from all pages
        - Instructions are clear and contain correct information
        - There are no grammar and spelling mistakes

* UI testing
    - Aim - Verifying the graphic user interface meets the specifications
    - Points to be checked:
        - Compliance with the standards of graphical interfaces
        - Testing with different screen resolutions
        - Testing the graphical user interface on target devices: smartphones and tablets.

* Cross-browser compatibility
    - Aim - Verify the correct work of the functionality on different browser configurations
    - Browsers to be checked:
        - Chrome
        - Safari
        - Firefox
    - The 3 browsers with the most market share according to "statcounter" are selected: https://gs.statcounter.com/browser-market-share#monthly-202001-202010-bar

* Performance testing
    - Aim - Determining how a system performs in terms of responsiveness
    - Points to be checked:
        - Page load speed testing
    - No other more specific performance tests will be made as only one functionality is in the scope

* Security testing
    - Aim - Verifying restricted data is accessible only to authorized users

* Mobile-friendly testing
    - Aim - Verifying functionality operates correctly on mobile devices
    - Points to be checked:
        - Verify the compatibility with smartphones and tablets
        - Optimize the loading time of the functionality
        - Ensure buttons are large enough for people with big finger
        - Optimize all images' size
        - Ensure your phone number is one click away from being dialed

* System Integration testing
    - No integration testing will be performed as only a single functionality is in the scope.

* User Acceptance testing
    - Aim - Verifying real users outside the team can't find any significant weak points in the functionality

##  Schedule
* Analyze software requirement specification - 4 days
* Create the Test Specification - 7 days
* Execute the test cases - 2 days
* Report the defects - 1 day

## Scope of testing
### Functionalities to be tested
Prerequisites: The functionalities are tested by a user registered in "telerikacademy.com" with no special access privileges
* Topic creation in a forum
### Functionalities not to be tested
* Forum sign in
* Leaving comments
* Push/e-mail notifications

## Exit Criteria
* All planned tests have been run.
* No critical or high severity defects are open.
* Successful execution of the user acceptance tests.
* Software development activities are completed within the projected cost.
* Software development activities are completed within the projected timelines.