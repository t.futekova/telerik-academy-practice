window.onscroll = function () {
  stickyMenu();
};

var header = document.getElementById("sticky_header");
var sticky = header.offsetTop;

function stickyMenu() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

//slideshows
$(".slideshow-dayana > div:gt(0)").hide();
setInterval(function () {
  $(".slideshow-dayana > div:first")
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo(".slideshow-dayana");
}, 3000);

$(".slideshow-nelly > div:gt(0)").hide();
setInterval(function () {
  $(".slideshow-nelly > div:first")
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo(".slideshow-nelly");
}, 3000);

$(".slideshow-teddy > div:gt(0)").hide();
setInterval(function () {
  $(".slideshow-teddy > div:first")
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo(".slideshow-teddy");
}, 3000);

$(".slideshow-rado > div:gt(0)").hide();
setInterval(function () {
  $(".slideshow-rado > div:first")
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo(".slideshow-rado");
}, 3000);

$(".slideshow-victor > div:gt(0)").hide();
setInterval(function () {
  $(".slideshow-victor > div:first")
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo(".slideshow-victor");
}, 3000);

//dark/light mode toggle
let count = 0;

const changeMode = () => {
  count++;

  //even odd click detect
  let isEven = function (someNumber) {
    return someNumber % 2 === 0 ? true : false;
  };
  // on odd clicks do this
  if (isEven(count) === false) {
    $(".mode").text("Light Mode on");
    $(".mode").css("color", "#c80e25");
    $('#sticky_header').css("background-color", "#978c8c");
    $("#container").css("background-color", "rgb(211 211 211");
    $("#index").css("background-color", "rgb(211 211 211");
    $("#header").css("background-color", "rgb(211 211 211");
    $("#footer").css("background-color", "#978c8c");
    $("#header h1").css("color", "black");
    $("#footer h4").css("color", "black");
    $(".memb").css("color", "black");

    document.cookie = "theme=light";
  }
  // on even clicks do this
  else if (isEven(count) === true) {
    $(".mode").text("Light Mode off");
    $(".mode").css("color", "#ccc");
    $('#sticky_header').css("background-color", "#333");
    $("#container").css("background-color", "rgb(27, 27, 27)");
    $("#index").css("background-color", "rgb(27, 27, 27)");
    $("#header").css("background-color", "rgb(27, 27, 27)");
    $("#footer").css("background-color", "black");
    $("#header h1").css("color", "#006e9f");
    $("#footer h4").css("color", "#006e9f");
    $(".memb").css("color", "#006e9f");

    document.cookie = "theme=dark";
  }
};

// Set cookie for light/dark theme
function getCookieValue(name) {
  let result = document.cookie.match("(^|[^;]+)\\s*" + name + "\\s*=\\s*([^;]+)")
  return result ? result.pop() : ""
}

function checkCookie() {
  const theme = getCookieValue("theme");
  if (theme === "light") {
      counter = 0;
      changeMode();
      changeModeT();
  } 
  else {
      counter = 1;
      changeMode();
      changeModeT();
  }
  
}

// Save light/dark theme toggle position in localStorage
var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {},
  $checkboxes = $("#switch :checkbox");

$checkboxes.on("change", function(){
$checkboxes.each(function(){
  checkboxValues[this.id] = this.checked;
});

localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
});

// On page load
$.each(checkboxValues, function(key, value) {
$("#" + key).prop('checked', value);
});

//Zoom over the buddy group img
$(function () {
  $(".column img").imgHover();
});
//Zoom over the buddy group img^

$(document).ready(function () {
  $("#the-usual-suspects").click(function () {
    window.location = "https://www.imdb.com/title/tt0114814/";
  });
  //this will be triggered when the first button was clicked
  $("#the-king-speech").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt1504320/";
  });
  $("#serial-weddings").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt2800240/";
  });
  $("#serial-weddings").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt2800240/";
  });

  $("#star-wars-a-new-hope").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0076759/";
  });

  $("#lord-of-the-rings").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0120737/";
  });

  $("#indiana-jones-and-the-raiders-of-the-lost-ark").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0082971/";
  });

  $("#coraline").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0327597/";
  });
  $("#spirited-away").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0245429/";
  });

  $("#the-nightmare-before-christmas").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0107688/";
  });

  $("#vikings").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt2306299/";
  });

  $("#aquaman").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt1477834/";
  });

  $("#avengers-endgame").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt4154796/";
  });

  $("#the-queen-gambit").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt10048342/";
  });

  $("#mr-robot").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt4158110/";
  });

  $("#breaking-bad").click(function () {
    //this will find the selected website from the dropdown
    window.location = "https://www.imdb.com/title/tt0903747/";
  });
});


// Back to top
$(document).ready(function(){

  $(window).scroll(function(){
    if($(this).scrollTop() > 40){
      $('#top-button').fadeIn();
    } else{
      $('#top-button').fadeOut();
    }
  });

  $("#top-button").click(function(){
    $('html ,body').animate({scrollTop : 0},800);
  });
});