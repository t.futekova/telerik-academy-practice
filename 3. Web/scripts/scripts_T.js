//dark/light mode toggle +
let counter = 0;

const changeModeT = () => {
  counter++;

  //even odd click detect
  let isEven = function (someNumber) {
    return someNumber % 2 === 0 ? true : false;
  };
  // on odd clicks do this
  if (isEven(counter) === false) {
    $(".mode").text("Light Mode on");
    $(".mode").css("color", "#006e9f");

    $('#banner').css("border-top", "0.5px solid rgb(90 90 90)");
    $('#banner').css("border-bottom", "0.5px solid rgb(90 90 90)");

    $('.banner-info').css("background-color", "rgb(174 174 174)");
    $('.banner-info').css("border", "0.5px solid rgb(90 90 90)");

    $(".poster-more-info").css("color", "black");
    $('#sticky_header').css("background-color", "rgb(174 174 174)");
    $('#sticky_header').css("border-bottom", "0.5px solid rgb(90 90 90)");

    $('.movie-box').css("border", "0.5px solid rgb(90 90 90)");
    $('.movie-box').css("background-color", "rgb(174 174 174)");

    $("#container").css("background-color", "rgb(211 211 211");
    $("#index").css("background-color", "rgb(211 211 211");
    $("#header").css("background-color", "rgb(211 211 211");
    $("#footer").css("background-color", "rgb(137 137 137)");
    $("#header h1").css("color", "black");
    $("#footer h4").css("color", "black");
    $(".title").css("background-color", "rgb(174 174 174)");
    $('.title').css("border", "0.5px solid rgb(90 90 90)");
    $('.title').css("color", "black");

    $('.movie-title').css("color", "black");
    $('.movie-content').css("color", "black");

    $('.border-line').css("border-bottom", "0.5px solid rgb(90 90 90)");

    $("#banner").css("background-color", "rgb(137 137 137)");
    $('.red').css("color", "#006e9f");

    document.cookie = "theme=light";
  }
  // on even clicks do this
  else if (isEven(counter) === true) {
    $(".mode").text("Light Mode off");
    $(".mode").css("color", "#ccc");

    $('#banner').css("border-top", "0.5px solid rgb(135 135 135)");
    $('#banner').css("border-bottom", "0.5px solid rgb(135 135 135)");

    $('.banner-info').css("background-color", "rgb(27, 27, 27)");
    $('.banner-info').css("border", "0.5px solid rgb(135 135 135)");

    $(".poster-more-info").css("color", "white");
    $("#container").css("background-color", "rgb(27, 27, 27)");
    $('#sticky_header').css("background-color", "#333");
    $('#sticky_header').css("border-bottom", "0.5px solid rgb(135 135 135)");

    $('.movie-box').css("border", "0.5px solid rgb(135 135 135)");
    $('.movie-box').css("background-color", "#333");

    $("#index").css("background-color", "rgb(27, 27, 27)");
    $("#header").css("background-color", "rgb(27, 27, 27)");
    $("#footer").css("background-color", "black");
    $("#header h1").css("color", "#006e9f");
    $("#footer h4").css("color", "#006e9f");
    $(".title").css("background-color", "#333");
    $('.title').css("border", "0.5px solid rgb(135 135 135)");
    $('.title').css("color", "#006e9f");

    $('.movie-title').css("color", "white");
    $('.movie-content').css("color", "white");

    $('.border-line').css("border-bottom", "0.5px solid rgb(135 135 135)");
   
    $("#banner").css("background-color", "black");
    $('.red').css("color", "#c80e25");

    document.cookie = "theme=dark";
  }
};


// Switch between gallery images
jQuery(document).ready(function($) {
    $('.img-gallery-1').on({'click': function(){
        const location = $(this).attr('src');
        $('.image-underlay-1').attr('src', location);
    }});

    $('.img-gallery-2').on({'click': function(){
        const location = $(this).attr('src');
        $('.image-underlay-2').attr('src', location);
    }});

    $('.img-gallery-3').on({'click': function(){
        const location = $(this).attr('src');
        $('.image-underlay-3').attr('src', location);
    }});
});

// Read more/less
const readMoreLess1 = () => {
    if ($('.dots.1').is(':hidden')) {
        $('.dots.1').css('display', 'inline');
        $('.read-more-less-btn.1').text('Read more');
        $('.more-text.1').css('display', 'none');
    } else {
        $('.dots.1').css('display', 'none');
        $('.read-more-less-btn.1').text('Read less');
        $('.more-text.1').css('display', 'inline');
    }
}
const readMoreLess2 = () => {
    if ($('.dots.2').is(':hidden')) {
        $('.dots.2').css('display', 'inline');
        $('.read-more-less-btn.2').text('Read more');
        $('.more-text.2').css('display', 'none');
    } else {
        $('.dots.2').css('display', 'none');
        $('.read-more-less-btn.2').text('Read less');
        $('.more-text.2').css('display', 'inline');
    }
}
const readMoreLess3 = () => {
    if ($('#dots.3').is(':hidden')) {
        $('.dots.3').css('display', 'inline');
        $('.read-more-less-btn.3').text('Read more');
        $('.more-text.3').css('display', 'none');
    } else {
        $('.dots.3').css('display', 'none');
        $('.read-more-less-btn.3').text('Read less');
        $('.more-text.3').css('display', 'inline');
    }
}

// Go to part of page by clicking poster
$(document).ready(function() {
    $('#first-poster').click(function() {
        window.location = './index_T.html#1';
    });

    $('#second-poster').click(function() {
        window.location = './index_T.html#2';
    });

    $('#third-poster').click(function() {
        window.location = './index_T.html#3';
    });
});

// Set cookie for light/dark theme
function getCookieValue(name) {
    let result = document.cookie.match("(^|[^;]+)\\s*" + name + "\\s*=\\s*([^;]+)")
    return result ? result.pop() : ""
}

function checkCookie() {
    const theme = getCookieValue("theme");
    if (theme === "light") {
        counter = 0;
        changeModeT();
        changeMode();
    } 
    else {
        counter = 1;
        changeModeT();
        changeMode();
    }
    
}

// Save light/dark theme toggle position in localStorage
var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {},
    $checkboxes = $("#switch :checkbox");

$checkboxes.on("change", function(){
  $checkboxes.each(function(){
    checkboxValues[this.id] = this.checked;
  });
  
  localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
});

// On page load
$.each(checkboxValues, function(key, value) {
  $("#" + key).prop('checked', value);
});