package com.telerikacademy.oop.interfacedemo;

public interface InterfaceExample {
    void sayHello();

    int CONSTANT = 1;

    default void avoid() {
        System.out.println("Avoid using default methods in Interfaces!");
    }

    static int increment(int x) {
        return x + 1;
    }
}
