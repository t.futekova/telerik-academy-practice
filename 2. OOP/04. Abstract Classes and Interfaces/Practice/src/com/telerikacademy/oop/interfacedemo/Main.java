package com.telerikacademy.oop.interfacedemo;

public class Main {
    public static void main(String[] args) {
        int number = InterfaceExample.CONSTANT;
        System.out.println(number);

        System.out.println(InterfaceExample.increment(number));

        InterfaceExample example = new InterfaceExampleImpl();
        example.sayHello();
        example.avoid();
    }
}
