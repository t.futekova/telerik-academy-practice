package com.telerikacademy.oop.interfaces;

import com.telerikacademy.oop.enums.Direction;

public interface Movable {
    void move(double distance, Direction direction);
}
