package com.telerikacademy.oop.enums;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
