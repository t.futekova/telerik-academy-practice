package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Color;
import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

public class Line implements Movable {
    private Point firstEnd;
    private Point secondEnd;
    private Color color;

    public Line(Point firstEnd, Point secondEnd, Color color) {
        this.firstEnd = firstEnd;
        this.secondEnd = secondEnd;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Line between %s and %s", firstEnd, secondEnd);
    }

    @Override
    public void move(double distance, Direction direction) {
        firstEnd.move(distance, direction);
        secondEnd.move(distance, direction);
    }
}
