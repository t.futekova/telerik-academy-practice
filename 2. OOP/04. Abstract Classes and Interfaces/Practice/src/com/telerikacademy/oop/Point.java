package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

public class Point implements Movable {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("{x: %.2f, y: %.2f}", x, y);
    }

    @Override
    public void move(double distance, Direction direction) {
        switch (direction) {
            case UP:
                y += distance;
                break;
            case DOWN:
                y -= distance;
                break;
            case RIGHT:
                x += distance;
                break;
            case LEFT:
                x -= distance;
                break;
            default:
                throw new IllegalArgumentException("Not a valid direction!");
        }
    }
}
