package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Color;
import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

public class Circle extends Shape implements Movable {
    private final double radius;
    private Point center;

    public Circle(Color color, double radius, Point circle) {
        super(color);
        this.radius = radius;
        this.center = circle;
    }

    public double getRadius() {
        return radius;
    }

    public Point getCenter() {
        return center;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return String.format("Circle with center: %s and radius: %.2f",
                getCenter(),
                getRadius());
    }

    @Override
    public void move(double distance, Direction direction) {
        center.move(distance, direction);
    }
}
