package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Color;
import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

public class Rectangle extends Shape implements Movable {
    private final double width;
    private final double height;
    private Point center;

    public Rectangle(Color color, double width, double height, Point center) {
        super(color);
        this.width = width;
        this.height = height;
        this.center = center;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Point getCenter() {
        return center;
    }

    @Override
    protected double getArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return String.format("Rectangle with center: %s, width: %.2f, height: %.2f",
                getCenter(),
                getWidth(),
                getHeight());
    }
    @Override
    public void move(double distance, Direction direction) {
        center.move(distance, direction);
    }
}
