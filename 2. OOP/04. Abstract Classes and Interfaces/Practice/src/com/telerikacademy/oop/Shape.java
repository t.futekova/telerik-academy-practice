package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Color;
import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

public abstract class Shape implements Movable {
    private final Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    protected abstract double getArea();

    @Override
    public void move(double distance, Direction direction) {

    }
}
