package com.telerikacademy.oop;

import com.telerikacademy.oop.enums.Color;
import com.telerikacademy.oop.enums.Direction;
import com.telerikacademy.oop.interfaces.Movable;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Shape> shapes = generateListOfShapes();
        List<Movable> movables = generateListOfMovable();
        for (Shape shape : shapes) {
            System.out.printf("The %s %s has area: %.2f %n",
                    shape.getColor().toString().toLowerCase(),
                    shape.getClass().getSimpleName(),
                    shape.getArea());
        }

        for (Movable movable : movables) {
            System.out.println("Before");
            System.out.println(movable);
            movable.move(1.5, Direction.UP);
            System.out.println("After");
            System.out.println(movable);
        }
    }

    public static List<Movable> generateListOfMovable() {
        Point center = new Point(4.5, 5.6);

        Movable circle = new Circle(Color.BLUE, 3.5, center);
        Movable rectangle = new Rectangle(Color.YELLOW, 6, 7, center);
        Point a = new Point(4, 6);
        Point b = new Point(6, 7);

        Movable line = new Line(a, b, Color.GREEN);

        List<Movable> movables = new ArrayList<>();
        movables.add(circle);
        movables.add(rectangle);
        movables.add(line);

        return movables;
    }

    public static List<Shape> generateListOfShapes() {
        Point center = new Point(4.5, 5.6);

        Shape circle = new Circle(Color.BLUE, 3.5, center);
        Shape rectangle = new Rectangle(Color.YELLOW, 6, 7, center);

        List<Shape> shapes = new ArrayList<>();
        shapes.add(circle);
        shapes.add(rectangle);

        return shapes;
    }
}
