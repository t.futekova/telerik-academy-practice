package com.telerikacademy.oop;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);
        setDescription(description);

        logEvent(String.format("Item created: %s", viewInfo()));
    }
    private void setDescription(String description) {
        if (description == null) {
            this.description = "No description";
        } else {
            this.description = description;
        }
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void revertStatus() {
        if (getStatus() != Status.VERIFIED) {
            logEvent("Issue status set to Open");
            setStatus(Status.OPEN);
        } else {
            logEvent("Issue status already Open");
        }
    }

    @Override
    public void advanceStatus() {
        if (getStatus() != Status.OPEN) {
            logEvent("Issue status set to Verified");
            setStatus(Status.VERIFIED);
        } else {
            logEvent("Issue status already Verified");
        }
    }

    @Override
    public String viewInfo() {
        return String.format("Issue: %s, Description: %s", super.viewInfo(), getDescription());
    }
}
