package com.telerikacademy.oop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardItem {

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private static final String TITLE_NULL_MESSAGE = "Title cannot be empty!";
    private static final String TITLE_INVALID_MESSAGE = "Title must be between 5 and 30 symbols!";

    private static final String DUE_DATE_INVALID_MESSAGE = "Due date cannot be in the past!";

    private static final String STATUS_NULL_MESSAGE = "Status cannot be empty!";
    private static final Status initialStatus = Status.OPEN;
    private static final Status finalStatus = Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;
    private final List<EventLog> history = new ArrayList<>();


    public BoardItem(String title, LocalDate dueDate) {
        this(title, dueDate, initialStatus);
    }

    public BoardItem(String title, LocalDate dueDate, Status status) {
        verifyTitleLengthAndIfNull(title);
        verifyIfDueDateIsInThePast(dueDate);

        this.title = title;
        this.dueDate = dueDate;
        this.status = status;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        verifyTitleLengthAndIfNull(title);
        logEvent(String.format("Title changed from %s to %s", this.title, title));
        this.title = title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        verifyIfDueDateIsInThePast(dueDate);
        logEvent(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));
        this.dueDate = dueDate;
    }
    public void setStatus(Status status) {
        if (status == null) {
            throw new IllegalArgumentException(STATUS_NULL_MESSAGE);
        }
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    private void verifyTitleLengthAndIfNull(String title) {
        if (title == null) {
            throw new IllegalArgumentException(TITLE_NULL_MESSAGE);
        }
        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException(TITLE_INVALID_MESSAGE);
        }
    }

    private void verifyIfDueDateIsInThePast(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(DUE_DATE_INVALID_MESSAGE);
        }
    }

    public abstract void revertStatus();

    public abstract void advanceStatus();

    public void logEvent(String event) {
        history.add(new EventLog(event));
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", getTitle(), getStatus(), getDueDate());
    }

    public String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog event : history) {
            builder.append(event.viewInfo()).append(System.lineSeparator());
        }

        return builder.toString();
    }

}
