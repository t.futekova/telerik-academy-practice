package com.telerikacademy.oop;

import java.time.LocalDate;

public class Task extends BoardItem {
    private static  final String ASSIGNEE_NULL_MESSAGE = "Assignee can't be null!";
    private static  final String ASSIGNEE_INVALID_MESSAGE = "Assignee must be between 5 and 30 characters!";

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, Status.TODO);
        verifyAssigneeLengthAndIfNull(assignee);
        this.assignee = assignee;

        logEvent(String.format("Item created: %s", viewInfo()));
    }

    public void setAssignee(String assignee) {
        verifyAssigneeLengthAndIfNull(assignee);
        logEvent(String.format("Assignee changed from %s to %s", this.assignee, assignee));
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    private void verifyAssigneeLengthAndIfNull(String assignee) {
        if (assignee == null) {
            throw new IllegalArgumentException(ASSIGNEE_NULL_MESSAGE);
        }
        if (assignee.length() < 5 || assignee.length() > 30) {
            throw new IllegalArgumentException(ASSIGNEE_INVALID_MESSAGE);
        }
    }

    @Override
    public void revertStatus() {
        if (getStatus() != Status.OPEN) {
            logEvent(String.format("Status changed from %s to %s",
                    getStatus(), Status.values()[getStatus().ordinal() - 1]));
            setStatus(Status.values()[getStatus().ordinal() - 1]);
        } else {
            logEvent("Issue status already Open");
        }
    }

    //
//    public void advanceStatus() {
//        if (status != finalStatus) {
//            logEvent(String.format("Status changed from %s to %s",
//                    status, Status.values()[status.ordinal() + 1]));
//            status = Status.values()[status.ordinal() + 1];
//        } else {
//            logEvent("Can't advance, already at Verified");
//        }
//    }
    @Override
    public void advanceStatus() {
        if (getStatus() != Status.VERIFIED) {
            logEvent(String.format("Status changed from %s to %s",
                    getStatus(), Status.values()[getStatus().ordinal() + 1]));
            setStatus(Status.values()[getStatus().ordinal() + 1]);
        } else {
            logEvent("Issue status already Verified");
        }
    }

    @Override
    public String viewInfo() {
        return String.format("Task: %s, Assignee: %s", super.viewInfo(), this.getAssignee());
    }

}
