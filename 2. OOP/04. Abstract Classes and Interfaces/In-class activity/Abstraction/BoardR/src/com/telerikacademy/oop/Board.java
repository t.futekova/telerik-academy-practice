package com.telerikacademy.oop;

import com.telerikacademy.oop.loggers.Logger;

import java.util.ArrayList;

public class Board {

    private static final ArrayList<BoardItem> items = new ArrayList<>();

    private Board() {}

    static void addItem(BoardItem item) {
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item is already in the list!");
        }
        items.add(item);
    }

    static String totalItems() { return String.format("%d", Board.items.size()); }

//    public static void displayHistory() {
//        for (BoardItem item : items) {
//            System.out.println(item.getHistory());
//        }
//    }

    public static void displayHistory(Logger logger) { // accept an Logger type
        for (BoardItem item : items) {
            // call the log() method and give it a string. (the viewHistory() method returns a String)
            logger.log(item.getHistory());
        }
    }

}
