package com.telerikacademy.oop;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);
        setDescription(description);

    }
    private void setDescription(String description) {
        if (description == null) {
            this.description = "No description";
        } else {
            this.description = description;
        }
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String viewInfo() {
        return String.format("Issue: %s, Description: %s", super.viewInfo(), this.getDescription());
    }
}
