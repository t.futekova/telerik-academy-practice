package com.telerikacademy.oop.loggers;

public interface Logger {

    void log(String value);

}
