package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private final static String COMMENT_DOES_NOT_EXIST = "Cannot remove comment! The comment does not exist!";

    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;
    private List<Comment> comments;

    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        this.make = make;
        this.model = model;
        this.price = price;
        this.vehicleType = vehicleType;
        comments = new ArrayList<>();
        validateState();
    }

    @Override
    public int getWheels() {
        return getType().getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void removeComment(Comment commentToRemove) {
        Validator.ValidateNull(commentToRemove, ModelsConstants.COMMENT_CANNOT_BE_NULL);
        boolean commentExists = false;
        for (Comment comment : comments) {
            if (commentToRemove.equals(comment)) {
                comments.remove(comment);
                commentExists = true;
            }
        }
        if (!commentExists) {
            throw new IllegalArgumentException(COMMENT_DOES_NOT_EXIST);
        }
    }

    @Override
    public void addComment(Comment commentToAdd) {
        Validator.ValidateNull(commentToAdd, ModelsConstants.COMMENT_CANNOT_BE_NULL);
        comments.add(commentToAdd);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MAKE_FIELD, make)).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, model)).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, vehicleType.getWheelsFromType())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    // protected methods can be accessed from the subclasses of the superclass no matter in which package they are;
    // as this is a method relevant only to the aforementioned subclasses and not to every class in the program,
    // the protected access modifier is more appropriate than public;
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }

    private void validateState() {
        Validator.ValidateNull(make, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, MAKE_FIELD));

        Validator.ValidateDecimalRange(make.length(), ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, MAKE_FIELD,
                        ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH));

        Validator.ValidateNull(model, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, MODEL_FIELD));

        Validator.ValidateDecimalRange(model.length(), ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, MODEL_FIELD,
                        ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH));

        Validator.ValidateDecimalRange(price, ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, PRICE_FIELD,
                        ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE));
    }

}
