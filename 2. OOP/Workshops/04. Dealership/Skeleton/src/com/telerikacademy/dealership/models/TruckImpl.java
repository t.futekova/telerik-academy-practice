package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private final static String CAPACITY_FIELD = "Weight Capacity";

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        this.weightCapacity = weightCapacity;
        validateState();
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %dt", CAPACITY_FIELD, weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    private void validateState() {
        Validator.ValidateDecimalRange(weightCapacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, CAPACITY_FIELD.replace("C", "c"),
                        ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
    }
}
