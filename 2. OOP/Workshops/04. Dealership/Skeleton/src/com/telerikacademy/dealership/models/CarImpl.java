package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {

    private final static String SEATS_FIELD = "Seats";

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        this.seats = seats;
        validateState();
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %d", SEATS_FIELD, seats);
    }

    @Override
    public int getSeats() {
        return seats;
    }

    private void validateState() {
        Validator.ValidateDecimalRange(seats, ModelsConstants.MIN_SEATS, ModelsConstants.MAX_SEATS,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, SEATS_FIELD,
                        ModelsConstants.MIN_SEATS, ModelsConstants.MAX_SEATS));
    }
}
