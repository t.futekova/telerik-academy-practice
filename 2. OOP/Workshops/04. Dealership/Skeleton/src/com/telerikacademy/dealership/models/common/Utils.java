package com.telerikacademy.dealership.models.common;

import java.math.BigDecimal;

public class Utils {
    
    public static String removeTrailingZerosFromDouble(double number) {
        BigDecimal num = BigDecimal.valueOf(number).stripTrailingZeros();
        return num.toPlainString();
    }
    
}
