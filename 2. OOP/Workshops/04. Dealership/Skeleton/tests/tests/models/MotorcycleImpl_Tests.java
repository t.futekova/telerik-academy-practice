package tests.models;

import com.telerikacademy.dealership.models.MotorcycleImpl;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MotorcycleImpl_Tests {
    
    @Test
    public void MotorcycleImpl_ShouldImplementMotorcycleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assertions.assertTrue(motorcycle instanceof Motorcycle);
    }
    
    @Test
    public void MotorcycleImpl_ShouldImplementVehicleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assertions.assertTrue(motorcycle instanceof Vehicle);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new MotorcycleImpl(null, "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("m", "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("makeMakeMakeMakeMake", "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new MotorcycleImpl("make", null, 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "modelModelModelM", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", -100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 1000001, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new MotorcycleImpl("make", "model", 100, null));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryLengthIsBelow_3() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 100, "dr"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryLengthIsAbove10() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 100, "12345678901"));
    }
    
}
