package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductBase implements Product {

    public static final String NAME_NULL_MESSAGE = "Name can't be null";
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final String NAME_INVALID_MESSAGE = "Name must be between 3 and 10 characters!";

    public static final String BRAND_NULL_MESSAGE = "Brand can't be null";
    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;
    public static final String BRAND_INVALID_MESSAGE = "Brand must be between 2 and 10 characters!";

    public static final String PRICE_INVALID_MESSAGE = "Price cannot be negative!";

    private String name;
    private String brand;
    private double price;
    private final GenderType gender;
    
    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }
    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException(NAME_NULL_MESSAGE);
        }
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = name;
    }
    private void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException(BRAND_NULL_MESSAGE);
        }
        if (brand.length() < BRAND_MIN_LENGTH || brand.length() > BRAND_MAX_LENGTH) {
            throw new IllegalArgumentException(BRAND_INVALID_MESSAGE);
        }
        this.brand = brand;
    }
    private void setPrice(double price) {
        if (price <= 0) {
            throw new IllegalArgumentException(PRICE_INVALID_MESSAGE);
        }
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s %n #Price: $%.2f %n #Gender: %s", getName(), getBrand(), getPrice(), getGender());
    }
}
