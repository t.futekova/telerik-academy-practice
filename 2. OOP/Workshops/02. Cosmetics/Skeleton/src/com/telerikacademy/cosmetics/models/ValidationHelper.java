package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.contracts.Product;

public class ValidationHelper {
    public static final String PRODUCT_NULL_MESSAGE = "Product can't be null";

    public static void checkProductNotNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_NULL_MESSAGE);
        }
    }
}
