package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    private static final String INGREDIENTS_NULL_MESSAGE = "Ingredients cannot be null";

    private List<String> ingredients;
    
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    public void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException(INGREDIENTS_NULL_MESSAGE);
        }
        this.ingredients = ingredients;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("%s %n Ingredients: %s %n ===", super.print(), getIngredients());
    }
}
