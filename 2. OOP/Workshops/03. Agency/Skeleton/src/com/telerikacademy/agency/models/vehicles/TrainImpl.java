package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private static final String PASSENGER_CAPACITY_INVALID_MESSAGE =
            "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final int MIN_PASSENGER_CAPACITY = 30;
    private static final int MAX_PASSENGER_CAPACITY = 150;

    private static final String CARTS_INVALID_MESSAGE =
            "A train cannot have less than 1 cart or more than 15 carts.";
    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    public void validatePassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
    }

    private void setCarts(int carts) {
        if (carts < MIN_CARTS || carts > MAX_CARTS) {
            throw new IllegalArgumentException(CARTS_INVALID_MESSAGE);
        }
        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String print() {
        return String.format("%s %n%s %nCarts amount: %d %n",
                printClassName(), super.print(), getCarts());
    }

    @Override
    public String toString() {
        return print();
    }

}
