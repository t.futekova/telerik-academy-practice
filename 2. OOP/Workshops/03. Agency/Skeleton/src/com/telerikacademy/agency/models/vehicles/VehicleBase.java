package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final String PASSENGER_CAPACITY_INVALID_MESSAGE =
            "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final int MIN_PASSENGER_CAPACITY = 1;
    private static final int MAX_PASSENGER_CAPACITY = 800;

    private static final String PRICE_PER_KILOMETER_INVALID_MESSAGE =
            "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;

    private static final String TYPE_CANT_BE_NULL = "Type can't be null!";

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        validatePassengerCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;

        validatePricePerKilometer(pricePerKilometer);
        this.pricePerKilometer = pricePerKilometer;

        validateType(type);
        this.type = type;
    }

    public VehicleType getType() {
        return type;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public void validatePassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
    }

    private void validatePricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER) {
            throw new IllegalArgumentException(PRICE_PER_KILOMETER_INVALID_MESSAGE);
        }
    }

    private void validateType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
    }

    public String printClassName() {
        return String.format("%s ----", getClass().getSimpleName().replace("Impl", ""));
    }

    public String print() {
        return String.format("Passenger capacity: %d %nPrice per kilometer: %.2f %nVehicle type: %s",
                getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    @Override
    public String toString() {
        return print();
    }
    
}
