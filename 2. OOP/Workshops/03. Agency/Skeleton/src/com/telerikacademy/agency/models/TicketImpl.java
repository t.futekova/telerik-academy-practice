package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private static final String ADMINISTRATIVE_COST_INVALID_MESSAGE =
            "Administrative cost can't be negative";

    private Journey journey;
    private double administrativeCosts;
    
    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        setAdministrativeCosts(administrativeCosts);
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        if (administrativeCosts <= 0) {
            throw new IllegalArgumentException(ADMINISTRATIVE_COST_INVALID_MESSAGE);
        }
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return getAdministrativeCosts() * journey.calculateTravelCosts();
    }

    @Override
    public String print() {
        return String.format("%s ---- %nDestination: %s %nPrice: %.2f %n",
                getClass().getSimpleName().replace("Impl", ""),
                getJourney().getDestination(),
                calculatePrice());
    }

    @Override
    public String toString() {
        return print();
    }
}
