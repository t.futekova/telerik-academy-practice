package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final String PASSENGER_CAPACITY_INVALID_MESSAGE =
            "A bus cannot have less than 10 passengers or more than 50 passengers.";
    private static final int MIN_PASSENGER_CAPACITY = 10;
    private static final int MAX_PASSENGER_CAPACITY = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    public void validatePassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
    }

    @Override
    public String print() {
        return String.format("%s %n%s %n",
                printClassName(), super.print());
    }

    @Override
    public String toString() {
        return print();
    }
}
