package com.telerikacademy.agency.tests.models.vehicles;

import com.telerikacademy.agency.models.vehicles.BusImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BusImpl_Tests {
    
    @Test
    public void constructor_should_throw_when_passengerCapacityLessThanMinValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(9, 2));
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityMoreThanMaxValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(51, 2));
    }
    
}
