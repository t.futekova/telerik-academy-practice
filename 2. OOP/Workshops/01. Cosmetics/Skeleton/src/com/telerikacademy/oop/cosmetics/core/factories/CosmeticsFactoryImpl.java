package com.telerikacademy.oop.cosmetics.core.factories;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.Product;

public class CosmeticsFactoryImpl implements CosmeticsFactory {
    
    public Category createCategory(String name) {
        return new Category(name);
    }
    
    public Product createProduct(String name, String brand, double price, String gender) {

        GenderType compare = GenderType.MEN;
        int flag = 0;

        for (GenderType g : GenderType.values()) {
            if (g.name().equalsIgnoreCase(gender)) {
                compare = g;
                flag++;
            }

        }

        if (flag == 0) {
            throw new IllegalArgumentException("Not a valid gender!");
        }

        return new Product(name, brand, price, compare);

    }
    
    public ShoppingCart createShoppingCart() {
        return new ShoppingCart();
    }
    
}
