package com.telerikacademy.oop.cosmetics.models;

public class Product {

    private static final int MIN_PRODUCT_NAME_LENGTH = 3;
    private static final int MAX_PRODUCT_NAME_LENGTH = 10;
    private static final int MIN_PRODUCT_BRAND_LENGTH = 2;
    private static final int MAX_PRODUCT_BRAND_LENGTH = 10;

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {

        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);

    }

    public double getPrice() {
        return price;
    }

    private void setName(String name) {

        if (name == null) {
            throw new IllegalArgumentException("Product name cannot be empty!");
        }

        if (name.length() < MIN_PRODUCT_NAME_LENGTH || name.length() > MAX_PRODUCT_NAME_LENGTH) {
            throw new IllegalArgumentException("Product name must be between 3 and 10 symbols!");
        }

        this.name = name;

    }

    private void setBrand(String brand) {

        if (brand == null) {
            throw new IllegalArgumentException("Product brand cannot be empty!");
        }

        if (brand.length() < MIN_PRODUCT_BRAND_LENGTH || brand.length() > MAX_PRODUCT_BRAND_LENGTH) {
            throw new IllegalArgumentException("Brand name must be between 2 and 10 symbols!");
        }

        this.brand = brand;

    }

    private void setPrice(double price) {

        if (price <= 0.00) {
            throw new IllegalArgumentException("Price cannot be less than 0");
        }

        this.price = price;

    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String print() {

        return String.format("%n #%s %s%n #Price: %.2f%n #Gender: %s%n ===", this.name, this.brand, this.price, this.gender);

    }

}
