package com.telerikacademy.oop.cosmetics.core.contracts;

import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.Product;

public interface CosmeticsFactory {
    
    Category createCategory(String name);
    
    Product createProduct(String name, String brand, double price, String gender);
    
    ShoppingCart createShoppingCart();
    
}