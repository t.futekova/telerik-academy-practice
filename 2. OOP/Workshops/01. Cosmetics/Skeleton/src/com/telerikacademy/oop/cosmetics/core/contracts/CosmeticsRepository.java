package com.telerikacademy.oop.cosmetics.core.contracts;

import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.Product;

import java.util.Map;

public interface CosmeticsRepository {
    
    ShoppingCart getShoppingCart();
    
    Map<String, Category> getCategories();
    
    Map<String, Product> getProducts();
    
}
