package com.telerikacademy.oop.cosmetics.models;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private final List<Product> productList;

    public ShoppingCart() { productList = new ArrayList<>(); }

    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {

        if (product == null) {
            throw new IllegalArgumentException("Product cannot be empty!");
        }
        productList.add(product);

    }

    public void removeProduct(Product product) {

        if (product == null) {
            throw new IllegalArgumentException("Product cannot be empty!");
        }

        for (int i = 0; i < productList.size(); i++) {

            if (product.equals(productList.get(i))) {
                productList.remove(productList.get(i));
                break;
            }

        }

    }

    public boolean containsProduct(Product product) {

        boolean containsProduct = false;

        if (product == null) {
            throw new IllegalArgumentException("Product cannot be empty!");
        }

        for (Product value : productList) {
            if (product.equals(value)) {
                containsProduct = true;
                break;
            }
        }

        return containsProduct;

    }

    public String getTotalPrice() {

        double totalPrice = 0;

        for (Product product : productList) {

            totalPrice += product.getPrice();

        }

        return String.valueOf(totalPrice);

    }

}
