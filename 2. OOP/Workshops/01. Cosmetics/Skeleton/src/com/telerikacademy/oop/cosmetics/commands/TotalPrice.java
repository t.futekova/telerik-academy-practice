package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.ShoppingCart;

import java.util.List;

public class TotalPrice implements Command {
    
    private CosmeticsRepository cosmeticsRepository;
    private CosmeticsFactory cosmeticsFactory;
    
    public TotalPrice(CosmeticsFactory cosmeticsFactory, CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsFactory = cosmeticsFactory;
        this.cosmeticsRepository = cosmeticsRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        ShoppingCart cart = cosmeticsRepository.getShoppingCart();
        if (cart.getProductList().size() == 0) {
            return "No product in shopping cart!";
        }
        return String.format(CommandConstants.TOTAL_PRICE_IN_SHOPPING_CART, cart.getTotalPrice());
    }
    
}
