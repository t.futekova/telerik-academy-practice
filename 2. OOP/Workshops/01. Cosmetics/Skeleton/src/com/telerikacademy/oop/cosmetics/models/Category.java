package com.telerikacademy.oop.cosmetics.models;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private static final int MIN_CATEGORY_NAME_LENGTH = 2;
    private static final int MAX_CATEGORY_NAME_LENGTH = 15;

    private String name;
    private List<Product> products;
    
    public Category(String name) {

        setName(name);
        products = new ArrayList<>();

    }
    
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    private void setName(String name) {

        if (name.length() < MIN_CATEGORY_NAME_LENGTH || name.length() > MAX_CATEGORY_NAME_LENGTH) {
            throw new IllegalArgumentException("Category must be between 2 and 15 symbols!");
        }
        this.name = name;

    }
    
    public void addProduct(Product product) {

        if (product == null) {
            throw new IllegalArgumentException("Product cannot be empty!");
        }

        products.add(product);

    }
    
    public void removeProduct(Product product) {

        boolean isRemoved = false;

        for (int i = 0; i < products.size(); i++) {

            if (product.equals(products.get(i))) {
                products.remove(products.get(i));
                isRemoved = true;
                break;
            }

        }

        if (!isRemoved) {
            throw new IllegalArgumentException("Product is not found!");
        }

    }
    
    public String print() {

        if (products.size() == 0) {

            return String.format("#Category: %s%n  #No product in this category", name);

        } else {

            String print = String.format("Category: %s", name);

            for (Product product : getProducts()) {

                print += product.print();

            }

            return print;

        }

    }
    
}
