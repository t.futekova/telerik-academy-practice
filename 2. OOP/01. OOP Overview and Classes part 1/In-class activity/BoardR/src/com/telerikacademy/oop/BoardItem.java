package com.telerikacademy.oop;

import java.time.LocalDate;

public class BoardItem {

    private static final int MIN_BOARD_ITEM_TITLE_LENGTH = 5;
    private static final int MAX_BOARD_ITEM_TITLE_LENGTH = 30;

    private final Status initialStatus = Status.OPEN;
    private final Status finalStatus = Status.VERIFIED;

    public String title;
    public LocalDate dueDate;
    public Status status;

    public BoardItem(String title, LocalDate dueDate) {

        if (title == null) {
            throw new IllegalArgumentException("Title cannot be empty!");
        }

        if (title.length() < MIN_BOARD_ITEM_TITLE_LENGTH || title.length() > MAX_BOARD_ITEM_TITLE_LENGTH) {
            throw new IllegalArgumentException("Title must be between 5 and 30 symbols!");
        }

        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Due date cannot be in the past!");
        }

        this.title = title;
        this.dueDate = dueDate;
        this.status = initialStatus;

    }

    public void revertStatus() {

        if (status != initialStatus) {
            status = Status.values()[status.ordinal() - 1];
        }

    }

    public void advanceStatus() {

        if (status != finalStatus) {
            status = Status.values()[status.ordinal() + 1];
        }

    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }

}
