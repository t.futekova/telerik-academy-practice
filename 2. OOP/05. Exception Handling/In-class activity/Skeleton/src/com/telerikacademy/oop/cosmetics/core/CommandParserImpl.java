package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.List;

public class CommandParserImpl implements CommandParser {

    public String parseCommand(String commandLine) {
        String commandName = commandLine.split(" ")[0];
        return commandName;
    }

    public List<String> parseParameters(String commandLine) {
        String[] commandParts = commandLine.split(" ");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i]);
        }
        return parameters;
    }
}
