package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.List;

public class CreateProduct implements Command {
    private static final String PRODUCT_CREATED = "Product with name %s was created!";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count

        String name = parameters.get(0);
        String brand = parameters.get(1);
        //TODO Validate price format
        double price = Double.parseDouble(parameters.get(2));
        //TODO Validate gender format
        GenderType gender = GenderType.valueOf(parameters.get(3).toUpperCase());

        result = createProduct(name, brand, price, gender);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        //TODO Ensure category name is unique

        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);

        return String.format(PRODUCT_CREATED, name);
    }
}
