package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;

import java.util.List;

public class CreateCategory implements Command {
    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final String CATEGORY_NAME_IS_NOT_UNIQUE = "Category %s already exist.";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        try {
            String categoryName = parameters.get(0);

            result = createCategory(categoryName);
        } catch (Exception e) {
            throw new IllegalArgumentException("CreateCategory command expects 1 parameter.");
        }
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {
        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new DuplicateEntityException(String.format(CATEGORY_NAME_IS_NOT_UNIQUE, categoryName));
        }
        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);

        return String.format(CATEGORY_CREATED, categoryName);
    }
}
