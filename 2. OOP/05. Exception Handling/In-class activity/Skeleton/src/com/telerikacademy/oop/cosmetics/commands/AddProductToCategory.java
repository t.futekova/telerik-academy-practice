package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;

import java.util.List;

public class AddProductToCategory implements Command {
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";

    private final ProductRepository productRepository;
    private String result;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        try {
            String categoryNameToAdd = parameters.get(0);
            String productNameToAdd = parameters.get(1);

            result = addProductToCategory(categoryNameToAdd, productNameToAdd);
        } catch (Exception e) {
            throw new IllegalArgumentException("AddProductToCategory command expects 2 parameters.");
        }
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {
        Category category;
        Product product;

        try {
            category = productRepository.getCategories().get(categoryName);
        } catch (Exception e) {
            return String.format("Category %s does not exist.", categoryName);
        }

        try {
            product = productRepository.getProducts().get(productName);
        } catch (Exception e) {
            return String.format("Product %s does not exist.", productName);
        }

        category.addProduct(product);
        return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
    }
}
