package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;

public class ProductImpl implements Product {

    private static final String NAME_CANT_BE_NULL = "Name can't be null";
    private static final String NAME_INVALID_MESSAGE = "Product name should be between %d and %d symbols.";
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;

    private static final String BRAND_CANT_BE_NULL = "Brand can't be null";
    private static final String BRAND_INVALID_MESSAGE = "Brand should be between %d and %d symbols.";
    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;

    private static final String PRICE_INVALID_MESSAGE = "Price can't be negative.";

    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException(NAME_CANT_BE_NULL);
        }
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(NAME_INVALID_MESSAGE, NAME_MIN_LENGTH, NAME_MAX_LENGTH));
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException(BRAND_CANT_BE_NULL);
        }
        if (brand.length() < BRAND_MIN_LENGTH || brand.length() > BRAND_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(BRAND_INVALID_MESSAGE, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH));
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException(PRICE_INVALID_MESSAGE);
        }
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
