package com.telerikacademy.oop.cosmetics.core.contracts;

import java.util.Map;

public interface ProductRepository {
    Map<String, Category> getCategories();

    Map<String, Product> getProducts();
}
