package com.telerikacademy.oop;

import com.telerikacademy.oop.exceptions.InvalidUserOperationException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number, divisor;
        try {
            System.out.println("Enter a number to divide:");
            number = scanner.nextInt();

            System.out.println("Enter a divisor:");
            divisor = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Integer number is expected!");
            return;
        } finally {
            System.out.println("User input done.");
        }

        try {
            int result = divide(number, divisor);

            System.out.printf("The result from the division of %d by %d is: %d%n",
                    number,
                    divisor,
                    result);
        } catch (InvalidUserOperationException e) {
            System.out.println(e.getMessage());
        }

    }

    private static int divide(int x, int y) {
        if (y == 0) {
            throw new InvalidUserOperationException("Dividing by 0 isn't allowed!");
        }
        return x / y;
    }
}
