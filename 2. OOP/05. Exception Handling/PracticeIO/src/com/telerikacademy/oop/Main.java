package com.telerikacademy.oop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
//        FileWriter writer = null;
//        try {
//            writer = new FileWriter("file.txt");
//            writer.write("Hello World!\n");
//            writer.write("I am writing to a file :)\n");
//            writer.flush();
//        } finally {
//            if (writer != null) {
//                writer.close();
//            }
//        }

        try (
                FileWriter writer = new FileWriter("file.txt")
        ){
            writer.write("Hello World!\n");
            writer.write("I am writing to a file :)\n");
            writer.flush();
        }

        try (
                FileReader reader = new FileReader("file.txt");
                BufferedReader bufferedReader = new BufferedReader(reader);
                ) {
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                System.out.println(line);
            }
        }

    }
}
