package com.telerikacademy.oop;

public class Editor extends Staff{
    private int numberOfArticles;

    public Editor(String name, int userID, int numberOfArticles) {
        super(name, userID);
        this.numberOfArticles = numberOfArticles;
    }

    public int getNumberOfArticles() {
        return numberOfArticles;
    }

    @Override
    public String toString() {
        return String.format("%s%nPublished %d articles", super.toString(), getNumberOfArticles());
    }
}
