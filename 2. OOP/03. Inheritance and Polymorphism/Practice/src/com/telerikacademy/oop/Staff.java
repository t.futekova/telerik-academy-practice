package com.telerikacademy.oop;

public class Staff extends User{

    private int staffID;

    public Staff(String name, int userID) {
        super(name);
        this.staffID = userID;
    }

    public int getStaffID() {
        return staffID;
    }

    @Override
    public String identify() {
        return String.format("%s #%d", super.identify(), getStaffID());
    }

    @Override
    public String toString() {
        return String.format("%s%nStaffID: %d", super.toString(), getStaffID());
    }
}
