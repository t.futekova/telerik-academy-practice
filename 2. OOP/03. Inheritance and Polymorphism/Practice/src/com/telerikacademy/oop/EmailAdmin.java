package com.telerikacademy.oop;

import java.util.ArrayList;
import java.util.List;

public class EmailAdmin extends Staff{
    private List<String> emails;

    public EmailAdmin(String name, int userID) {
        super(name, userID);
        emails = new ArrayList<>();
    }

    public List<String> getEmails() {
        return new ArrayList<>(emails);
    }

    public void addEmails(String email){
        emails.add(email);
    }

    @Override
    public String toString() {
        return String.format("%s ");
    }
}
