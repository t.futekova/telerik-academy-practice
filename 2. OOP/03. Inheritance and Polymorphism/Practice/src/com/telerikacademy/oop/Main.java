package com.telerikacademy.oop;

public class Main {

    public static void main(String[] args) {
        User[] listOfUsers = {
                new User("Ivan"),
                new Staff("Georgi", 1),
                new Editor("Petar", 2, 13)};

        for (User user : listOfUsers) {
            System.out.println(user);
            System.out.println("####");
        }

    }
}
