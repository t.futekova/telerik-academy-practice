package com.telerikacademy.oop;

import java.time.LocalDate;

public class Task extends BoardItem {
    private static  final String ASSIGNEE_NULL_MESSAGE = "Assignee can't be null!";
    private static  final String ASSIGNEE_INVALID_MESSAGE = "Assignee must be between 5 and 30 characters!";

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate);
        verifyAssigneeLengthAndIfNull(assignee);
        this.assignee = assignee;
    }

    public void setAssignee(String assignee) {
        verifyAssigneeLengthAndIfNull(assignee);
        logEvent(String.format("Assignee changed from %s to %s", this.assignee, assignee));
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    private void verifyAssigneeLengthAndIfNull(String assignee) {

        if (assignee == null) {
            throw new IllegalArgumentException(ASSIGNEE_NULL_MESSAGE);
        }
        if (assignee.length() < 5 || assignee.length() > 30) {
            throw new IllegalArgumentException(ASSIGNEE_INVALID_MESSAGE);
        }

    }

    @Override
    public Status initialStatus() {
        return Status.TODO;
    }
}
