package com.telerikacademy.oop;

import java.time.LocalDate;
import java.util.Objects;

public class Issue extends BoardItem {

    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        setDescription(description);

    }
    private void setDescription(String description) {
        this.description = Objects.requireNonNullElse(description, "No description");
    }

    @Override
    public Status initialStatus() {
        return Status.OPEN;
    }

}
