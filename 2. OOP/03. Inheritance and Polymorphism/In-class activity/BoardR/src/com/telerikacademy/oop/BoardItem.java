package com.telerikacademy.oop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem extends InitialStatus {

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private static final String TITLE_NULL_MESSAGE = "Title cannot be empty!";
    private static final String TITLE_INVALID_MESSAGE = "Title must be between 5 and 30 symbols!";

    private static final String DUE_DATE_INVALID_MESSAGE = "Due date cannot be in the past!";

    private final Status finalStatus = Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;
    private final List<EventLog> history;

    public BoardItem(String title, LocalDate dueDate) {

        verifyTitleLengthAndIfNull(title);
        verifyIfDueDateIsInThePast(dueDate);

        this.title = title;
        this.dueDate = dueDate;
        this.status = initialStatus();
        history = new ArrayList<>();
        logEvent(String.format("Item created: %s", viewInfo()));

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        verifyTitleLengthAndIfNull(title);

        logEvent(String.format("Title changed from %s to %s", this.title, title));

        this.title = title;

    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {

        verifyIfDueDateIsInThePast(dueDate);

        logEvent(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));

        this.dueDate = dueDate;

    }

    public Status getStatus() {
        return status;
    }

    public List<EventLog> getHistory() {
        return new ArrayList<>(history);
    }

    private void verifyTitleLengthAndIfNull(String title) {

        if (title == null) {
            throw new IllegalArgumentException(TITLE_NULL_MESSAGE);
        }

        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException(TITLE_INVALID_MESSAGE);
        }

    }

    private void verifyIfDueDateIsInThePast(LocalDate dueDate) {

        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(DUE_DATE_INVALID_MESSAGE);
        }

    }

    @Override
    public Status initialStatus() {
        return Status.OPEN;
    }

    public void revertStatus() {

        if (status != initialStatus()) {
            logEvent(String.format("Status changed from %s to %s",
                    status, Status.values()[status.ordinal() - 1]));

            status = Status.values()[status.ordinal() - 1];
        } else {
            logEvent("Can't revert, already at Open");
        }

    }

    public void advanceStatus() {

        if (status != finalStatus) {
            logEvent(String.format("Status changed from %s to %s",
                    status, Status.values()[status.ordinal() + 1]));
            status = Status.values()[status.ordinal() + 1];
        } else {
            logEvent("Can't advance, already at Verified");
        }

    }

    public void logEvent(String event) {
        history.add(new EventLog(event));
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", getTitle(), getStatus(), getDueDate());
    }

    public void displayHistory() {

        for (int i = 0; i < history.size(); i++) {
            System.out.println(getHistory().get(i).viewInfo());
        }

    }

}
