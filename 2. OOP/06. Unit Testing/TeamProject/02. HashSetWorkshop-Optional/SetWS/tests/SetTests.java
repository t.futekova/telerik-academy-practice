import com.telerikacademy.set.MyHashSet;
import com.telerikacademy.set.MyHashSetImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SetTests {

    private MyHashSet<String> set;

    @BeforeEach
    public void before() {
        set = new MyHashSetImpl<>();
    }

    private void addSomeElementsToSet() {
        set.add("John");
        set.add("Seth");
        set.add("Mike");
    }

    @Test
    public void add_should_NotIncreaseSize_when_ElementAlreadyInSet() {
        addSomeElementsToSet();
        set.add("John");
        Assertions.assertEquals(3, set.size());
    }
    @Test
    public void add_should_IncreaseSetSize_when_ElementNotInSet() {
        addSomeElementsToSet();
        Assertions.assertEquals(3, set.size());
    }
    @Test
    public void remove_should_ThrowException_when_ElementNotInSet() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> set.remove("John"));
    }
    @Test
    public void remove_should_DecreaseSetSize_when_ElementAlreadyInSet() {
        addSomeElementsToSet();
        set.remove("John");
        Assertions.assertEquals(2, set.size());
    }
    @Test
    public void contains_should_ReturnTrue_when_ElementAlreadyInSet() {
        addSomeElementsToSet();
        Assertions.assertTrue(set.contains("Mike"));
    }
    @Test
    public void contains_should_ReturnFalse_when_ElementNotInSet() {
        addSomeElementsToSet();
        Assertions.assertFalse(set.contains("David"));
    }
    @Test
    public void resize_should_DoubleCapacity_when_ThresholdIsReached() {
        set.add("A");
        set.add("B");
        set.add("C");
        set.add("D");
        set.add("E");
        set.add("F");
        set.add("G");
        set.add("H");
        set.add("I");
        set.add("J");
        set.add("K");
        set.add("L");
        set.add("M");
        Assertions.assertEquals(32, set.capacity());
    }

}
