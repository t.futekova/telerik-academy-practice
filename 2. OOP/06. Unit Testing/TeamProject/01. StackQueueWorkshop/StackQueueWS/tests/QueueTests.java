import com.telerikacademy.Queue;
import com.telerikacademy.QueueImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class QueueTests {

    private Queue<Integer> testQueue;

    @BeforeEach
    public void before() {
        testQueue = new QueueImpl<>();
    }

    private void initializeQueueWithElements() {
        ArrayList<Integer> elements = new ArrayList<>();
        elements.add(7);
        elements.add(9);
        elements.add(4);
        testQueue = new QueueImpl<>(elements);
    }

    @Test
    public void constructor_should_CreateEmptyQueue_when_Initialized() {
        Assertions.assertEquals(0, testQueue.size());
    }
    @Test
    public void constructor_should_CreateQueue_when_ListOfElementsIsPassed() {
        initializeQueueWithElements();
        Assertions.assertEquals(3, testQueue.size());
    }
    @Test
    public void offer_should_SaveElements_when_ElementsAreGiven() {
        testQueue.offer(12);
        Assertions.assertEquals(12, testQueue.peek());
    }
    @Test
    public void poll_should_ThrowException_when_QueueIsEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> testQueue.poll());
    }
    @Test
    public void poll_should_RemoveFirstElement_when_QueueHasElements() {
        initializeQueueWithElements();
        testQueue.poll();
        Assertions.assertEquals(9, testQueue.peek());
    }
    @Test
    public void isEmpty_should_ReturnTrue_when_QueueIsEmpty() {
        Assertions.assertTrue(testQueue.isEmpty());
    }
    @Test
    public void isEmpty_should_ReturnFalse_when_QueueHasElements() {
        initializeQueueWithElements();
        Assertions.assertFalse(testQueue.isEmpty());
    }
    @Test
    public void peek_should_ThrowException_when_QueueIsEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> testQueue.peek());
    }
    @Test
    public void peek_should_ReturnLastElement_when_QueueHasElements() {
        initializeQueueWithElements();
        Assertions.assertEquals(7, testQueue.peek());
    }
    @Test
    public void size_should_ReturnUpdatedQueueSize_when_ElementsAreOffered() {
        testQueue.offer(4);
        testQueue.offer(7);
        Assertions.assertEquals(2, testQueue.size());
    }
    @Test
    public void size_should_ReturnUpdatedQueueSize_when_ElementsArePolled() {
        initializeQueueWithElements();
        testQueue.poll();
        Assertions.assertEquals(2, testQueue.size());
    }
}
