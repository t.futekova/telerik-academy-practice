import com.telerikacademy.StackImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class StackTests {
    private StackImpl<Integer> testStack;

    @BeforeEach
    public void before() {
        this.testStack = new StackImpl<>();
    }

    private void initializeStackWithElements() {
        ArrayList<Integer> elements = new ArrayList<>();
        elements.add(3);
        elements.add(5);
        elements.add(8);
        testStack = new StackImpl<>(elements);
    }

    @Test
    public void constructor_should_CreateEmptyStack_when_Initialized() {
        Assertions.assertEquals(0, testStack.size());
    }
    @Test
    public void constructor_should_CreateStack_when_ListOfElementsIsPassed() {
        initializeStackWithElements();
        Assertions.assertEquals(3, testStack.size());
    }
    @Test
    public void push_should_SaveElements_when_ElementsAreGiven() {
        testStack.push(8);
        Assertions.assertEquals(8, testStack.peek());
    }
    @Test
    public void pop_should_ThrowException_when_StackIsEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.pop());
    }
    @Test
    public void pop_should_RemoveLastElement_when_StackHasElements() {
        initializeStackWithElements();
        testStack.pop();
        Assertions.assertEquals(5, testStack.peek());
    }
    @Test
    public void peek_should_ThrowException_when_StackIsEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.peek());
    }
    @Test
    public void peek_should_ReturnLastElement_when_StackHasElements() {
        initializeStackWithElements();
        Assertions.assertEquals(8, testStack.peek());
    }
    @Test
    public void size_should_ReturnUpdatedStackSize_when_ElementsArePushed() {
        testStack.push(3);
        testStack.push(5);
        testStack.push(8);
        Assertions.assertEquals(3, testStack.size());
    }
    @Test
    public void size_should_ReturnUpdatedStackSize_when_ElementsArePopped() {
        initializeStackWithElements();
        testStack.pop();
        Assertions.assertEquals(2, testStack.size());
    }
    @Test
    public void isEmpty_should_ReturnTrue_when_StackIsEmpty() {
        Assertions.assertTrue(testStack.isEmpty());
    }
    @Test
    public void isEmpty_should_ReturnFalse_when_StackHasElements() {
        initializeStackWithElements();
        Assertions.assertFalse(testStack.isEmpty());
    }

}
