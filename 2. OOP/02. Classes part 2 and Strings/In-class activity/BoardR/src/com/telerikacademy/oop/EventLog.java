package com.telerikacademy.oop;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public final class EventLog {

    private final String description;
    private final LocalDateTime timestamp;

    public EventLog(String description) {

        if (description == null) {
            throw new IllegalArgumentException("Description cannot be empty!");
        }
        this.description = description;
        this.timestamp = LocalDate.now().atTime(LocalTime.now());

    }

    public String getDescription() {
        return this.description;
    }

    public String viewInfo() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");
        return String.format("[%s] %s", formatter.format(timestamp), description);
    }

}
