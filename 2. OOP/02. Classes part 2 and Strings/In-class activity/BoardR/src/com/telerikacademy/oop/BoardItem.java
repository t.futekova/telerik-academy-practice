package com.telerikacademy.oop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {

    private static final int MIN_BOARD_ITEM_TITLE_LENGTH = 5;
    private static final int MAX_BOARD_ITEM_TITLE_LENGTH = 30;

    private final Status initialStatus = Status.OPEN;
    private final Status finalStatus = Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;
    private final List<EventLog> eventLogs;

    public BoardItem(String title, LocalDate dueDate) {

        verifyTitleLengthAndIfNull(title);
        verifyIfDueDateIsInThePast(dueDate);

        this.title = title;
        this.dueDate = dueDate;
        this.status = initialStatus;
        eventLogs = new ArrayList<>();
        EventLog log = new EventLog(String.format("Item created: '%s', [%s | %s]", title, initialStatus, dueDate));
        eventLogs.add(log);

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        verifyTitleLengthAndIfNull(title);

        EventLog log = new EventLog(String.format("Title changed from %s to %s", this.title, title));
        eventLogs.add(log);
        this.title = title;

    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {

        verifyIfDueDateIsInThePast(dueDate);

        EventLog log = new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));
        eventLogs.add(log);

        this.dueDate = dueDate;

    }

    public Status getStatus() {
        return status;
    }

    public List<EventLog> getEventLogs() {
        return new ArrayList<>(eventLogs);
    }

    private void verifyTitleLengthAndIfNull(String title) {

        if (title == null) {
            throw new IllegalArgumentException("Title cannot be empty!");
        }

        if (title.length() < MIN_BOARD_ITEM_TITLE_LENGTH || title.length() > MAX_BOARD_ITEM_TITLE_LENGTH) {
            throw new IllegalArgumentException("Title must be between 5 and 30 symbols!");
        }

    }

    private void verifyIfDueDateIsInThePast(LocalDate dueDate) {

        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Due date cannot be in the past!");
        }

    }

    public void revertStatus() {

        if (status != initialStatus) {
            EventLog log = new EventLog(String.format("Status changed from %s to %s",
                    status, Status.values()[status.ordinal() - 1]));
            eventLogs.add(log);
            status = Status.values()[status.ordinal() - 1];
        } else {
            EventLog log = new EventLog("Can't revert, already at Open");
            eventLogs.add(log);
        }

    }

    public void advanceStatus() {

        if (status != finalStatus) {
            EventLog log = new EventLog(String.format("Status changed from %s to %s",
                    status, Status.values()[status.ordinal() + 1]));
            eventLogs.add(log);
            status = Status.values()[status.ordinal() + 1];
        } else {
            EventLog log = new EventLog("Can't advance, already at Verified");
            eventLogs.add(log);
        }

    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }

    public void displayHistory() {

        for (int i = 0; i < eventLogs.size(); i++) {
            System.out.println(getEventLogs().get(i).viewInfo());
        }

    }

}
