package com.telerikacademy.oop;

import java.util.ArrayList;

public class Board {

    private static final ArrayList<BoardItem> items = new ArrayList<>();

    private Board() {}

    static void addItem(BoardItem item) {
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item is already in the list!");
        }
        items.add(item);
    }

    static String totalItems() { return String.format("count: %d", Board.items.size()); }

}
