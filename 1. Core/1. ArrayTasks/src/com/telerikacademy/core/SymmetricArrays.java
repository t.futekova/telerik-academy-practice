package com.telerikacademy.core;

import java.util.ArrayList;
import java.util.Scanner;

public class SymmetricArrays {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            boolean isSymmetric = true;
            String line = scanner.nextLine();
            String[] inputStrArr = line.split(" ");
            if (inputStrArr.length == 0) {
                result.add("Yes");
            } else {
                for (int j = 0; j < inputStrArr.length / 2; j++) {
                    if (!inputStrArr[j].equals(inputStrArr[inputStrArr.length - j - 1])) {
                        isSymmetric = false;
                        break;
                    }
                }
                if (isSymmetric) {
                    result.add("Yes");
                } else {
                    result.add("No");
                }
            }
        }
        for (String elements : result) {
            System.out.println(elements);
        }

    }
}
