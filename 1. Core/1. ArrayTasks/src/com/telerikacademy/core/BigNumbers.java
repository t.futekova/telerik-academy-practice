package com.telerikacademy.core;

import java.util.Scanner;

public class BigNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] rowStrArr = scanner.nextLine().split(" ");
        int row1 = Integer.parseInt(rowStrArr[0]);
        int row2 = Integer.parseInt(rowStrArr[1]);
        int maxRow = Math.max(row1, row2);
        String[] strArray1 = scanner.nextLine().split(" ");
        String[] strArray2 = scanner.nextLine().split(" ");
        int[] intArray1 = new int[maxRow];
        int[] intArray2 = new int[maxRow];
        StringBuilder result = new StringBuilder();
        boolean carryOver = false;
        for (int i = 0; i < maxRow; i++) {
            int sum = 0;
            if (carryOver) {
                sum += 1;
            }
            if (row1 <= maxRow && i < row1) {
                intArray1[i] = Integer.parseInt(strArray1[i]);
            }
            if (row2 <= maxRow && i < row2) {
                intArray2[i] = Integer.parseInt(strArray2[i]);
            }
            sum += intArray1[i] + intArray2[i];
            if (sum >= 10) {
                sum -= 10;
                carryOver = true;
            } else {
                carryOver = false;
            }
            result.append(sum).append(" ");
        }
        result.setLength(result.length() - 1);
        System.out.println(result);
    }
}
