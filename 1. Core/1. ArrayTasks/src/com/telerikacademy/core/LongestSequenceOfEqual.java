package com.telerikacademy.core;

import java.util.Scanner;

public class LongestSequenceOfEqual {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] input = new int[n];
        for (int i = 0; i < input.length; i++) {
            input[i] = Integer.parseInt(scanner.nextLine());
        }
        int maxCounter = 0;
        int counter = 1;
        for (int i = 0; i < n - 1; i++) {
            if (input[i] == input[i + 1]) {
                counter += 1;
            } else {
                counter = 1;
            }
            if (maxCounter < counter) {
                maxCounter = counter;
            }

        }
        System.out.println(maxCounter);
    }
}
