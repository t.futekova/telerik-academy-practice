package com.telerikacademy.core;

import java.util.Scanner;

public class Bounce {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] inputStrArr = scanner.nextLine().split(" ");
        int n = Integer.parseInt(inputStrArr[0]);
        int m = Integer.parseInt(inputStrArr[1]);
        if (n == 1 || m == 1) {
            System.out.println("1");
        } else {
            long[][] matrix = new long[n][m];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    long matrixCell = (long) (Math.pow(2, i) * Math.pow(2, j));
                    matrix[i][j] = matrixCell;
                }
            }
            long sum = 0L;
            String direction = "down right";
            int row = 0;
            int col = 0;
            for (int i = 0; i < n * m; i++) {
                if (direction.equals("down right")) {
                    sum += matrix[row][col];
                    if (row == n - 1 && col != m - 1) {
                        row--;
                        col++;
                        direction = "up right";
                    } else if (col == m - 1 && row != n - 1){
                        row++;
                        col--;
                        direction = "down left";
                    } else if (row == n - 1 && col == m - 1) {
                        break;
                    }else {
                        row++;
                        col++;
                    }
                }
                if (direction.equals("up right")) {
                    sum += matrix[row][col];
                    if (col == m - 1 && row != 0) {
                        row--;
                        col--;
                        direction = "up left";
                    } else if (row == 0 && col != m - 1) {
                        row++;
                        col++;
                        direction = "down right";
                    } else if (col == m - 1 && row == 0) {
                        break;
                    }else {
                        row--;
                        col++;
                    }
                }
                if (direction.equals("up left")) {
                    sum += matrix[row][col];
                    if (row == 0 && col != 0) {
                        row++;
                        col--;
                        direction = "down left";
                    } else if (row == 0 && col == 0) {
                        break;
                    }else {
                        row--;
                        col--;
                    }
                }
                if (direction.equals("down left")) {
                    sum += matrix[row][col];
                    if (col == 0 && row != n - 1) {
                        row++;
                        col++;
                        direction = "down right";
                    } else if (row == n - 1 && col == 0) {
                        break;
                    }else {
                        row++;
                        col--;
                    }
                }

            }
            System.out.println(sum);
        }
    }
}
