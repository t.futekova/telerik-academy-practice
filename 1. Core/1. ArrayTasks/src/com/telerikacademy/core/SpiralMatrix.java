package com.telerikacademy.core;

import java.util.Scanner;

public class SpiralMatrix {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[n][n];
        String direction = "right";
        int row = 0;
        int col = 0;
        int turns = 0;
        for (int i = 1; i <= n * n; i++) {
            if (direction.equals("right")) {
                matrix[row][col] = i;
                if (col == n - 1 - turns) {
                    direction = "down";
                    row++;
                    continue;
                } else {
                    col ++;
                }
            }
            if (direction.equals("down")) {
                matrix[row][col] = i;
                if (row == n - 1 - turns) {
                    direction = "left";
                    col--;
                    continue;
                } else {
                    row++;
                }
            }
            if (direction.equals("left")) {
                matrix[row][col] = i;
                if (col == turns) {
                    direction = "up";
                    turns++;
                    row--;
                    continue;
                } else {
                    col--;
                }
            }if (direction.equals("up")) {
                matrix[row][col] = i;
                if (row == turns) {
                    direction = "right";
                    col++;
                } else {
                    row--;
                }
            }
        }
        for (int[] rows : matrix) {
            for (int cell : rows) {
                System.out.printf("%d ", cell);
            }
            System.out.println();
        }
    }
}
