package com.telerikacademy.core;

import java.util.Scanner;

public class ReverseArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] input = line.split(" ");
        int[] inputIntArr = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            inputIntArr[i] = Integer.parseInt(input[i]);
        }
        StringBuilder result = new StringBuilder();
        for (int i = inputIntArr.length - 1; i >= 0; i--) {
            result.append(inputIntArr[i]).append(", ");
        }

        result.setLength(result.length() - 2);
        System.out.println(result);
    }
}
