package com.telerikacademy.core;

import java.util.Scanner;

public class AboveTheMainDiagonal2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        long result = 0L;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                result += Math.pow(2, i) * Math.pow(2, j);
            }
        }
        System.out.println(result);
    }
}