package com.telerikacademy.core;

import java.util.Scanner;

public class ThreeGroups {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] inputStrArr = line.split(" ");
        int[] inputIntArr = new int[inputStrArr.length];
        StringBuilder result0 = new StringBuilder();
        StringBuilder result1 = new StringBuilder();
        StringBuilder result2 = new StringBuilder();
        for (int i = 0; i < inputStrArr.length; i++) {
            inputIntArr[i] = Integer.parseInt(inputStrArr[i]);
            if (inputIntArr[i] % 3 == 0) {
                result0.append(inputIntArr[i]).append(" ");
            } else if (inputIntArr[i] % 3 == 1) {
                result1.append(inputIntArr[i]).append(" ");
            } else {
                result2.append(inputIntArr[i]).append(" ");
            }
        }
        System.out.println(result0);
        System.out.println(result1);
        System.out.println(result2);
    }
}
