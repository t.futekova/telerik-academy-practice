package com.telerikacademy.core;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Order order1 = new Order("Teddy", Currency.BGN, LocalDate.now());

        order1.items.add(new Product("Keyboard", 100));
        order1.items.add(new Product("PC", 1000));

        Order order2 = new Order("Lili", Currency.EUR, LocalDate.now());

        order2.items.add(new Product("Gundam", 200));
        order2.items.add(new Product("Ink bottle", 20));

        Order[] orders = {order1, order2};

        for (Order order : orders) {
            order.displayGeneralInfo();
            System.out.println();
            order.displayItems();
            System.out.printf("%n ----------------------------- %n");
        }
    }
}
