package com.telerikacademy.core;

import java.time.LocalDate;
import java.util.ArrayList;

public class Order {

    String recipient;
    Currency currency;
    LocalDate deliveryOn;
    ArrayList<Product> items;

    public Order(String recipient, Currency currency,
                 LocalDate deliveryOn) {
        this.recipient = recipient;
        this.currency = currency;
        this.deliveryOn = deliveryOn;
        items = new ArrayList<>();
    }

    public void displayItems() {
        if (items.size() == 0) {
            System.out.println("No items");
        }

        StringBuilder builder = new StringBuilder("Items: ");
        for (Product item : items) {
            builder.append(String.format(" %s", item.getDisplayProductInfo()));
        }

        System.out.println(builder.toString());
    }

    public void displayGeneralInfo() {
        System.out.printf("Recipient: %s | Deliver on: %s | Total: %.2f",
                recipient, deliveryOn.toString(), calculateTotalPrice());
    }

    public double calculateTotalPrice() {
        double total = 0;
        for (Product product : items) {
            total += product.price;
        }

        if (currency == Currency.EUR) {
            total *= 2;
        }
        return total;
    }

}
